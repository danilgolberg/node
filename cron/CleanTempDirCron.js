const cron = require('node-cron');
const CronService = require('../services/CronService');

const cleanTempDir = cron.schedule('* * * * *', async () =>  {
    console.log('start task');
    await CronService.cleanTempDir()
}, {
  scheduled: false
});

module.exports = cleanTempDir