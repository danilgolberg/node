const mongoose = require('mongoose');    


const Schema =  mongoose.Schema;

const userScheme = new Schema({
    
    firstName: String,
    lastName: String
});


module.exports = mongoose.model('User',userScheme);

