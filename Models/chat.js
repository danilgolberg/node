const sequelize = require('../db')
const {Model,DataTypes} = require('sequelize');

  class Chat extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Chat.init({
    id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Chats',
  });

  module.exports = {
    Chat
}