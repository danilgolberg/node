const sequelize = require('../db')
const {Model,DataTypes} = require('sequelize')

class ContrieModel extends Model {}

ContrieModel.init({
  
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  icon: {
    type: DataTypes.JSON,
    
    
    },
 
}, {
tableName: 'contries',
  sequelize, 
  modelName: 'ContrieModel' 
});

module.exports = {
    ContrieModel: ContrieModel

}
