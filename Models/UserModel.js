const sequelize = require('../db')

const {Model,DataTypes} = require('sequelize');
const UserRoles = require('../services/UserRoles');

class UserModel extends Model {}

UserModel.init({
  
  firstName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  lastName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false
    
    },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
    select: false
    
  },
  role: {
    type: DataTypes.ENUM(UserRoles.ADMIN,UserRoles.FREELANCER,UserRoles.EMPLOYER),
    allowNull: false,
    
  },
  description: {
    type: DataTypes.STRING,
    
    
  },
  rate: {
    type: DataTypes.STRING,
    
    
  },
  position: {
    type: DataTypes.STRING,
    
    
  },
  avatar: {
    type: DataTypes.JSON
    
    
  },
  country_id: {
    type: DataTypes.INTEGER,
    
    
  },
  level_id: {
    type: DataTypes.INTEGER,
    
    
    },
  },
   
 {
  defaultScope: {
    attributes: {exclude: ['password']},
  },
scopes: {
  withPassword: {
    attributes: {},
  }
},
tableName: 'users',
sequelize, 
modelName: 'UserModel' 
});

module.exports = {
    UserModel: UserModel

}
