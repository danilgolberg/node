const sequelize = require('../db')
const {Model,DataTypes} = require('sequelize')

class LanguageModel extends Model {}

LanguageModel.init({
  
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },

 
}, {
tableName: 'languages',
  sequelize, 
  modelName: 'LanguageModel' 
});

module.exports = {
    LanguageModel: LanguageModel

}
