const sequelize = require('../db')
const {Model,DataTypes} = require('sequelize')

class LevelModel extends Model {}

LevelModel.init({
  
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },

}, {
tableName: 'levels',
  sequelize, 
  modelName: 'LevelModel' 
});

module.exports = {
    LevelModel: LevelModel

}
