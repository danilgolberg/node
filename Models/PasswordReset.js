const sequelize = require('../db')
const {Model,DataTypes} = require('sequelize')

class PasswordReset extends Model {}

PasswordReset.init({
  
 
  email: {
    type: DataTypes.STRING,
    allowNull: false
    
    },
  token: {
    type: DataTypes.STRING,
    allowNull: false,
   
    
  },
  
  },
 

 {
  indexes: [ {
    unique: true,
    fields: ['token']
  },

  ],
tableName: 'password_resets',
sequelize, 
modelName: 'PasswordReset' 
});

module.exports = {
  PasswordReset: PasswordReset

}
