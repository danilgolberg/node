const sequelize = require('../db')
const {Model,DataTypes} = require('sequelize');
class ChatUser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ChatUser.init({
    chat_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER
  }, {
    sequelize,
    tableName: 'chat_user',
    modelName: 'ChatUser',
    timestamps: false
  });

  ChatUser.removeAttribute('id')

  module.exports = {
    ChatUser
}