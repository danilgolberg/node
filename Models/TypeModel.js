const sequelize = require('../db')
const {Model,DataTypes} = require('sequelize')

class TypeModel extends Model {}

TypeModel.init({
  
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },

 
}, {
tableName: 'types',
  sequelize, 
  modelName: 'TypeModel' 
});

module.exports = {
    TypeModel: TypeModel

}
