const { ChatUser } = require("../Models/ChatUser")

class ChatController{
    async getChats(req,res){
console.log(req.user.id)
        const chats = await ChatUser.findAll({
            where:{
                user_id:req.user.id
            }
        })
        return res.json(chats)
}

}
module.exports = new ChatController()