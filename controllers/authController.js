const { request } = require("express");
const {UserModel} = require('../models/UserModel');
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');

class AuthController{
async singUp(req,res){
    console.log(req.body)
    const saltRounds = 10;
    const salt = bcrypt.genSaltSync(saltRounds);
    let hash = bcrypt.hashSync(req.body.password, salt);
    const user = await UserModel.create({ 
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password:hash,
        role: req.body.role
     });
    delete(user.dataValues.password)
    return res.json(user)
}
    async login(req,res){
      const {email,password} = req.body
      
      let user = await UserModel.scope('withPassword').findOne({
        where: {
           email: email
        
          }
      })
      if(!user){
        return res.status(422).json({
        errors: {
          email: "Wrong password or email."
        }
    
        })
      }
      let checkPassword = bcrypt.compareSync(password, user.password)
      if (!checkPassword  ){
      return res.status(422).json({
        errors: {
        email: "Wrong password or email."
      }
      })
}

      const token = jwt.sign({ id: user.id }, process.env.SECRET_KEY,{
      expiresIn: "7d"});
      return res.json({token:token})
}
}
module.exports = new AuthController()