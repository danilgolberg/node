const UserMongo = require('../Models/UserMongo')

class MongoController{
    async getAll(req,res){
        let user = new UserMongo();
        user.firstName = 'An';
        user.lastName = 'M';
        await user.save();

        let users = await UserMongo.find();
        return res.json(users); 
    }
}
 

module.exports = new MongoController()