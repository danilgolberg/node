const GenerateUniqueToken = require('../services/generateUniqueToken')
const sequelize = require('../db')
const {PasswordRecoveryEmail} = require('../mails/passwordRecoveryEmail')
const {UserModel} = require("../models/UserModel");
const {PasswordReset} = require('../Models/PasswordReset');


class PasswordRecoveryController{
     passwordForgot = async (req, res) => {
        const user = await UserModel.findOne({
            where:{
                email:req.body.email
            }
        })

        if(user){
            const t = await sequelize.transaction();
            try{
            await PasswordReset.destroy({
                where:
                {
                    email:req.body.email
                },
                transaction: t
            },)
            let existingToken = null
            let token = null
            do {
             token = GenerateUniqueToken.generateToken()
                existingToken = await PasswordReset.findOne({
                    where: {
                        token:token
                    }
                })
            }
            while (existingToken !== null)
            await PasswordReset.create({email: user.email, token:token})
            { transaction: t }
            let email = new PasswordRecoveryEmail()
            await email.send(user.email,token),
            await t.commit();
        }catch(e){
            await t.rollback();
            return res.status(500)({
                status:'error',
                message: 'Server error. Try again later'
            })
        }
        }
return res.json({status:'success'})
    }
}

module.exports = new PasswordRecoveryController()