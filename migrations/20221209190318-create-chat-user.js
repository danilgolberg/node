'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('chat_user', {
      chat_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'chats',
          
          },
          key: 'id'
        },
      },
      user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'users',
          },
          key: 'id'
        },
      },
     
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('chat_users');
  }
};