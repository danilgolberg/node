const winston = require("winston");
const moment = require('moment');


module.exports = winston.createLogger({
    transports: [
      new winston.transports.Console(),
      new winston.transports.File({ 
        level: 'error',
        filename: './logs/' + moment().format('DD-MM-YYYY') + '-log',
        format:  winston.format.combine(
            winston.format.timestamp({format:'DD-MM-YYYY HH:mm:ss'}),
            winston.format.align(),
            winston.format.printf(info => '${info.level}: ${[info.timestamp]}: ${info.message}')
        )
     })
    ]
  });