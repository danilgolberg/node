const {Mail} = require('./mail')

class PasswordRecoveryEmail extends Mail{
    async send(to,token) {
    const url = process.env.FRONT_URL + '/password/reset?email=' + to  + '&token=' + token


console.log(to)
        await this.transporter.sendMail({
            from: this.from,
            to: to,
            subject: "Hello ✔", // Subject line
            text: "Email for password forgot",
            html: 'For password recovery go to <a href="' + url + '">link</a>',
          });
    }
}

module.exports= {
    PasswordRecoveryEmail:PasswordRecoveryEmail
}