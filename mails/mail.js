const nodemailer = require("nodemailer");
class Mail {
    constructor() {
        this.from = process.env.MAIL_FROM
        this.transporter = nodemailer.createTransport({
            host: process.env.SMTP_HOST,
            port: process.env.SMTP_PORT,
            secure: false, // true for 465, false for other ports
            auth: {
              user: process.env.SMTP_USERNAME, // generated ethereal user
              pass: process.env.SMTP_PASSWORD, // generated ethereal password
            },
          });
    }
}

module.exports = {
    Mail: Mail 
}