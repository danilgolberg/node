const {validationResult} = require("express-validator");
const {check} = require("express-validator");
const {UserModel} = require('../models/UserModel');
const {validator} = require('./BaseValidator')
class LoginValidator extends validator {
    createValidators = () => [
        check('password')
        .trim()
        .escape()
        .not()
        .isEmpty()
        .isLength({min: 3, max: 50}),
        check('email')
        .trim()
        .escape()
        .not()
        .isEmpty()
        .withMessage("Email is required")
        .isLength({min: 3, max: 150})
        .isEmail()
        .custom(async(value) => {
            console.log(value)
           let user = await UserModel.findOne({
                where: {
                    email:value
                }
            }) 
            console.log(user)
            if (user){
                throw new Error('Email is already in use');
            }
            return true;
        }),
    ]
}

module.exports = {
    validate : new LoginValidator()
}