const UserRoles = require("../services/UserRoles");
const {UserModel} = require("../models/UserModel");
const {check} = require("express-validator");
const {validator} = require('./BaseValidator')
const path = require('path')
const fs =require('fs')
const {promisify} =require('util')
const unlinkAsync = promisify(fs.unlink)

class ProfileValidator extends validator {
    createValidators = () => [
        check('avatar').custom(async(value,{req}) => {
         {
                if(!req.files.avatar){
                    throw new Error('Field avatar is required');
                }
            }
            const fileData = req.fils.avatar[0]
            
            const permittedMimeTypes = ['image/jpeg', 'image/png'];

            if(!permittedMimeTypes.includes(fileData.mimeType)){
                await unlinkAsync(req.file.path)
                throw new Error('Upload image');
            }
            
            const maxSize = 8 * 1024 *1024;
            if(fileData.size > maxSize){
                await unlinkAsync(fileData.path)
                throw new Error('Vax size of file can be 8')
            } 
            return true;
            
        })
    ]
}

let val = new ProfileValidator()

module.exports = {
    validate: val
}