const {validationResult} = require("express-validator");
const {check} = require("express-validator");
const {UserModel} = require('../models/UserModel');
const {validator} = require('./BaseValidator')
const UserRoles = require('../services/UserRoles');
class SingUpValidator extends validator {
    createValidators = () => [
        check('firstName')
        .trim()
        .escape()
        .not()
        .isEmpty()
        .bail()
        .withMessage("firstName is required")
        .isLength({min: 3, max: 50})
        .withMessage("firstName must be between 3 and 50"),
        check('lastName')
        .trim()
        .escape()
        .not()
        .isEmpty()
        .bail()
        .withMessage("lastName is required")
        .isLength({min: 3, max: 50})
        .withMessage("firstName must be between 3 and 50"),
        check('password')
        .trim()
        .escape()
        .not()
        .isEmpty()
        .isLength({min: 3, max: 50}),
        check('email')
        .trim()
        .escape()
        .not()
        .isEmpty()
        .withMessage("firstName is required")
        .isLength({min: 3, max: 150})
        .isEmail()
        .custom(async(value) => {
            console.log(value)
           let user = await UserModel.findOne({
                where: {
                    email:value
                }
            }) 
            console.log(user)
            if (user){
                throw new Error('Email is already in use');
            }
            return true;
        }),
        check('role')
        .trim()
        .escape()
        .not()
    .isEmpty()
    .isIn([UserRoles.ADMIN,UserRoles.FREELANCER,UserRoles.EMPLOYER])
    .withMessage("Role must be one of" + UserRoles.FREELANCER + ','+ UserRoles.EMPLOYER),

]
}

module.exports = {
    validate : new SingUpValidator()
}