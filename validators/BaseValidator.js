const {validationResult} = require("express-validator")

class BaseValidator {
    constructor(){

        this.reporter = (req,res,next) => {
            const errors = validationResult(req);
            
            if (!errors.isEmpty()) {
                let validarionErrors = {}
                for (const error of errors.array()){
                    validarionErrors[error.param] = error.msg
                }
                return res.status(400).json({errors: validarionErrors});
            }
            next()
        }
        
        this.add = () => {
            return [
                this.createValidators(),
                this.reporter
            ]
        }
    }
    
    createValidators = () => []
} 
    
module.exports = {
    validator : BaseValidator
}