const express = require('express')
const app = express()
const sequelize = require('./db');
require('dotenv').config()
const router = require('./routes/index')
const {UserModel} = require('./models/UserModel');
const {ContrieModel} = require('./models/ContrieModel');
const {LevelModel} = require('./models/LevelModel');
const {LanguageModel} = require('./models/LanguageModel');
const  {Message} = require('./models/message');
const {ChatUser} = require('./models/ChatUser');
const {TypeModel} = require('./models/TypeModel');
const upload = require('multer');
const logger = require('./logger/logger');
const {engine} = require('express-handlebars');
const cronKernel = require('./cron/CronKernel');
const socket = require("socket.io");
const jwt = require("jsonwebtoken");
const mongoose = require('mongoose');             
var cors = require('cors')

const port = process.env.PORT || 3000
app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.engine('handlebars', engine());
app.set('view engine', 'handlebars');
app.set('views', './views');

app.use(express.static('public'));
app.use(cors())

app.use('/',router)

const  start = async () => {
  try {
    await mongoose.connect('mongodb://127.0.0.1:27017/kurs')
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
    
    
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
  
const server = app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
const io = socket(server);
 
io.use((socket, next) =>{
  const token = socket.handshake.auth.token;
  console.log(token)
  if (token) {
    try {
      const decoded = jwt.verify(token,process.env.SECRET_KEY);
      socket.handshake.auth.id = decoded.id
      console.log(decoded)
      return next()
    } catch (error) {
      const err = new Error("Not authentificated")
      next(err);      
      }
  }
    const err = new Error("Not authentificated")
    next(err);      
})



io.on("connection", (socket) => {
  const userId = socket.handshake.auth.id;
  console.log('userId', userId)
 
  socket.join(userId);
  console.log('Connected')
  socket.emit('first_emit',{userId:1})
  socket.on("disconnect", (reason) => {
    console.log('Disconnected')
  });

  socket.on("sendMessage",async(data) => {
    console.log(data)
    
    const message = {
      sender_id:userId,
      chat_id:data.chatId,
      text: data.text
    }
    await Message.create({
     

    })
    const chatUsers = await ChatUser.findAll({
      where: {
        chat_id: data.chatId
      }
    })
    for (const chatUser of chatUsers){
      io.to(chatUser.user_id).emit('getMessage', message);

    }
  });
  });
  

 

}
start()
//cronKernel()

app.use(function(req,res,next){
  let error =new Error('Not found');
  error.status = 404;
  next(error)
});

if(process.env.DEBUG_MOD === 'true'){
  app.use(function(err,req,res,next){
    res.status(err.status || 500);
    res.json({
     message: err.message,
     error: err.stack 
    })
   });
}

app.use(function(err,req,res,next){
  logger.error(JSON.stringify(err.message + err.stack));
 res.status(err.status || 500);
 return res.json({
  message: 'Server error. Try again later'
 })
});