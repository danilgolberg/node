    const jwt = require("jsonwebtoken");

    module.exports = function(req, res, next) {

    let token = req.headers.authorization?.split(' ')[1]
console.log(token)
   if(token){
    try {

        const decoded = jwt.verify(token,process.env.SECRET_KEY);
        req.user = decoded
        return next()
    } catch (error) {

        return res.status(401).json(
    
            {
                status: "error",
                message: "Unauthenticated" 
            }
            )
        }
    }
   
}