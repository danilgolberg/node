const fs = require('fs')
const path = require('path')
const moment = require("moment")
const {promisify} =require('util')
const unlinkAsync = promisify(fs.unlink)
const logger = require('../logger/logger');

class CronService {

    cleanTempDir = async () => {
        const directory = path.join(__dirname, '../storage/temp')
        
      fs.readdir(directory, async function (err, files) {
            if (err) {
                logger.error(JSON.stringify(err.message + err.stack));
            }
            else {
                
                for (const file of files) {
                    if (file === '.girignore') {
                        continue
                    }
                    let filePath = directory +'/' + file 
                    
                    let {birthtime} = fs.statSync(filePath)
                    let createDate = moment(birthtime)
                    let startTime = moment()
                    let duration = moment.duration(startTime.diff(createDate));
                    let hours = duration.asHours();
                    
                    if (hours > 4) {
                        await unlinkAsync(filePath)
                    }
                    
                }
            }   
        })
     }
}

module.exports = new CronService()