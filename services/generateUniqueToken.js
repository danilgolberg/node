const crypto = require('node:crypto');
class GenerateUniqueToken {
generateToken() {
    return crypto.randomBytes(30).toString('hex')
}
}

module.exports = new GenerateUniqueToken()