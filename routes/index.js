const Router = require('express')
const router = new Router()
const apiRateLimiterMiddleware = require('../middleware/apiRateLimiterMiddleware')
const mainRouter = require('./mainRouter')
const testRouter = require('./testRouter')


router.use('/', apiRateLimiterMiddleware, mainRouter)
router.use('/test', apiRateLimiterMiddleware, testRouter)

module.exports = router
