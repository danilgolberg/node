const Router = require('express')
const router = new Router()
const multer  = require('multer')
const path = require('path')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        const folder = path.dirname(require.main.filename)
        const absolutePath = path.join(folder,'storage/temp/')
      cb(null, absolutePath)
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname))
    }
})
  
  
const upload = multer({ storage: storage })

const ChatController  = require('../controllers/chatController')


    
const authMiddleware = require('../middleware/authMiddleware')
const asyncHandler = require('express-async-handler')

const AuthController = require('../controllers/authController')
const UserController = require('../controllers/userController')
const passwordRecoveryController = require("../controllers/passwordRecoveryController")
const MongoController = require("../controllers/MongoController")


const SingUpValidator = require('../validators/SingUpValidator')
const ProfileValidator = require('../validators/ProfileValidator')
const LoginValidator = require('../validators/LoginValidator')
const StaticPageController = require('../controllers/StaticPageController')


router.post('/singup',[SingUpValidator.validate.add()], AuthController.singUp)
router.post('/login',asyncHandler(AuthController.login))
router.post('/password/forgot',passwordRecoveryController.passwordForgot)
router.get('/users',[authMiddleware],UserController.getAll)
router.post('/profile', [upload.fields([{name:'avatar'},{name:'banner_photo'}]),ProfileValidator.validate.add()], UserController.profile)
router.get('/documentation', StaticPageController.documentation);
router.get('/chats',[authMiddleware], ChatController.getChats);
router.get('/mongo/users', MongoController.getAll);



module.exports = router


